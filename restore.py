#!/usr/bin/env python2
from xml.etree import ElementTree as ET
import libvirt
import sys
import string
import random
#import requests

if len(sys.argv)<>2:
    print "VM's name not defined"
    sys.exit(1)

vm=sys.argv[1];
conn = libvirt.open(None)
if conn == None:
    print 'Failed to open connection to the hypervisor'
    sys.exit(1)

try:
    dom0 = conn.lookupByName(vm)
    #destroy vm if running
#    dom0.destroy()
    
except:
    print 'Failed to find the main domain'
    sys.exit(1)


#dom0.suspend();
print "Restoring "+vm+' ...';
conn.restore('/sleep/'+vm+'.save');
dom0.resume();
