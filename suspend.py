#!/usr/bin/env python2
from xml.etree import ElementTree as ET
import libvirt
import sys
import string
import random
#import requests

conn = libvirt.open(None)
if conn == None:
    print 'Failed to open connection to the hypervisor'
    sys.exit(1)



#sys.exit(1)


domains=conn.listAllDomains(1)

print "Detected "+str(len(domains))+" active VMs"

if len(domains):
	print "Saving VMs states to disk:"
	for domain in domains:
    		print domain.name()+"..."
		domain.suspend();
		domain.save('/sleep/'+domain.name()+'.save');
 


