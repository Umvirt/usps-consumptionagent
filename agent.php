#!/usr/bin/env php
<?php
include('config.php');

function decho($str){
global $config;
if($config['debug']){
echo $str;
}
}

//Timeout setting for GET request
$ctx = stream_context_create(array('http'=>
    array(
        'timeout' => 10,
    )
));

$buff=@file_get_contents($url, false, $ctx);
$data=@json_decode($buff);
date_default_timezone_set($config['timezone']);
decho("Date: ".date("Y-m-d H:i:s")."\n");
if(@$data->sps->battery_voltage){
decho ("Source: ".$data->src."\n");
decho ("PV Voltage: ".$data->sps->pv_voltage."\n");
decho ("Battery Voltage: ".$data->sps->battery_voltage."\n");


$decission="powersave";
if($data->src=="PV" && $data->sps->pv_voltage>=$config['performance_voltage']){
$decission="performance";
}else{
if($data->sps->battery_voltage<=$config['low_voltage']){
decho ("LOW VOLTAGE\n");
$decission="poweroff";
}else{
decho ("NORMAL VOLTAGE\n");
}
}

}else{
decho("ERROR: Fetching SPS operational data was failed!");
$decission="powersave";
}

decho ("\nDecision: $decission\n");

switch($decission){
case "performance":
case "poweroff":
$cmd=$cmds[$decission];
break;
default:
$cmd=$cmds['powersave'];
}

decho ("Command to execute: $cmd\n");
if($config['execute']){
	echo exec($cmd)."\n\n";
}


